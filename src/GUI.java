import acm.graphics.GImage;
import acm.program.Program;

import acm.graphics.*;      // GCanvas
import javax.swing.*;       // JButton, JTextField, Jwhatever
import java.awt.*;          // Font, colors


import java.awt.event.ActionEvent;    // Event
import java.io.InputStream;


public class GUI extends Program
    //-----------------------------------------------Font Setup--------------------------------------
{
    private static Font fontAwesome;

    static {
        try (InputStream in = GUI.class.getClassLoader().getResourceAsStream("VertigoPlusFLF.ttf")) {
            fontAwesome = Font.createFont(Font.TRUETYPE_FONT, in);
        } catch (FontFormatException | java.io.IOException e) {
            System.out.print("select new font");
        }
    }
    Font labelFont = fontAwesome.deriveFont(48f);
    Font degreeFont = fontAwesome.deriveFont(30f);
    Font buttonFont = fontAwesome.deriveFont(32f);

    private JTextField zipField;
    private GImage mapImage;
    private GCanvas canvas;

    //----------Global Positioning Variables (GPV not GPS  hehe)----------------

    private int width = 750;
    private int height = 1260;
    private int radarMapX = 350;
    private int radarMapY = 175;
    //Ryan Offsets bubble text

    double offSetX1 = 30;
    double offSetX2 = 5;
    double offSetX3 = 70;
    double offSetY1 = 85;
    double offSetY2 = 140;
    double offSetY3 = 140;
    double xConstant = 230;

    //----------Globals for ACM----------------
    JLabel tempF1= new JLabel(), tempC1 = new JLabel(), cond1 = new JLabel(), tempF2 = new JLabel(), tempC2 = new JLabel(), tempF3 = new JLabel(), tempC3 = new JLabel();
    JLabel tempF4 = new JLabel(), tempC4 = new JLabel(), tempF5 = new JLabel(), tempC5 = new JLabel(), tempF6 = new JLabel(), tempC6 = new JLabel();
    JLabel tempF7 = new JLabel(), tempC7 = new JLabel(), tempF8 = new JLabel(), tempC8 = new JLabel(), tempF9 = new JLabel(), tempC9 = new JLabel();
    JLabel tempF10 = new JLabel(), tempC10 = new JLabel();
    JLabel day1 = new JLabel(),day2 = new JLabel(), day3 = new JLabel(), day4 = new JLabel(), day5 = new JLabel(), day6 = new JLabel(), day7 = new JLabel(), day8 = new JLabel(), day9 = new JLabel(), day10 = new JLabel();
    GImage main$map;
    GImage day1Cond, day2Cond, day3Cond, day4Cond, day5Cond, day6Cond, day7Cond, day8Cond, day9Cond, day10Cond, grid = new GImage("Images/rectgrid.png");;
    JButton goButton, mapButton;
    String inputBox;

    GUI()
    {
        start();
        setSize(width, height);
    }

    public void init()
    {

        main$map = new GImage("Images/current.png");
        canvas = new GCanvas();
        add(canvas);

        //Labels
        zipField = new JTextField(); zipField.setSize(200, 50);
        TextPrompt textPrompt = new TextPrompt("Enter Zip Code", zipField); textPrompt.changeAlpha(.75f);
        textPrompt.setHorizontalAlignment(JTextField.CENTER);
        zipField.setBackground(new Color(255, 255, 255, 128));
        zipField.setFont(labelFont);
        zipField.setHorizontalAlignment(JTextField.CENTER);
        cond1.setSize(200, 50);

        //Button Setups
        goButton = new JButton("Go"); goButton.setActionCommand("go");
        goButton.setBackground(Color.DARK_GRAY);
        goButton.setFont(buttonFont);
        goButton.setForeground(Color.WHITE);
        goButton.setBounds(0, 0, 0, 0);
        goButton.setSize(50, 50);

        mapButton = new JButton("map"); mapButton.setActionCommand("map");
        mapButton.setFont(buttonFont);
        mapButton.setForeground(Color.WHITE);
        mapButton.setBackground(Color.DARK_GRAY);

        canvas.add(zipField, 20, 140);

        canvas.add(goButton, 220, 140);

        canvas.add(mapButton, 240, 400);



        //----------------BackGround----------------------
        GImage background = new GImage("Images/ezgif.gif");

        canvas.add(background, 0, 0);

        canvas.add(main$map, 15, 120);


        canvas.add(grid, 15, 530);
        main$map.setVisible(false);
        grid.setVisible(false);
        mapButton.setVisible(false);
        addActionListeners();

    }


    public void actionPerformed(ActionEvent ae)
    {
        String whichButton = ae.getActionCommand();

        switch (whichButton)
        {
            case "go":
            {
                inputBox = zipField.getText();
                Weather w = new Weather(inputBox);
                zipField.setText("");
                w.getForecast();
                main$map.setVisible(true);
                grid.setVisible(true);
                mapButton.setVisible(true);


                //current observation bubble

                day1Cond = new GImage(w.day0Gif); canvas.add(day1Cond, 105, 260);
                tempF1.setText("High: " + w.jseDay0.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF1.setFont(labelFont); tempF1.setForeground(Color.DARK_GRAY); canvas.add(tempF1, 105, 345);
                tempC1.setText("Low: " + w.jseDay0.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempC1.setFont(buttonFont); tempC1.setForeground(Color.BLACK); canvas.add(tempC1,110, 400);
                cond1.setText(w.jseDay0.getAsJsonObject().get("conditions").getAsString());
                cond1.setFont(degreeFont); cond1.setForeground(Color.WHITE); canvas.add(cond1, 105, 205);
                day1.setText("Today, " + w.jseDay0.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay0.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day1.setFont(labelFont); day1.setForeground(Color.WHITE); canvas.add(day1, 90, 460);
                day1.setHorizontalAlignment(SwingConstants.CENTER);

                //day 2 observation
                day2Cond = new GImage(w.day1Gif); canvas.add(day2Cond, 80, 570);
                tempF2.setText(w.jseDay1.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay1.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF2.setFont(labelFont); tempF2.setForeground(Color.DARK_GRAY); canvas.add(tempF2, 80, 570+offSetY1);
                //tempC2.setText(w.jseDay1.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC2.setFont(buttonFont); tempC2.setForeground(Color.BLACK); canvas.add(tempC2,85, 570+offSetY2);
                day2.setText(w.jseDay1.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay1.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay1.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day2.setFont(buttonFont); day2.setForeground(Color.WHITE); canvas.add(day2, 90, 525);
                day2.setHorizontalAlignment(SwingConstants.CENTER);

                //day 3 observation
                day3Cond = new GImage(w.day2Gif); canvas.add(day3Cond, 80+xConstant, 570);
                tempF3.setText(w.jseDay2.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay2.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF3.setFont(labelFont); tempF3.setForeground(Color.DARK_GRAY); canvas.add(tempF3,80+xConstant, 570+offSetY1);
                //tempC3.setText(w.jseDay2.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC3.setFont(buttonFont); tempC3.setForeground(Color.BLACK); canvas.add(tempC3,85+xConstant, 570+offSetY2);
                day3.setText(w.jseDay2.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay2.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay2.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day3.setFont(buttonFont); day3.setForeground(Color.WHITE); canvas.add(day3, 90+xConstant, 525);
                day3.setHorizontalAlignment(SwingConstants.CENTER);

                //day 4 observation
                day4Cond = new GImage(w.day3Gif); canvas.add(day4Cond, 80+xConstant*2, 570);
                tempF4.setText(w.jseDay3.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay3.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF4.setFont(labelFont); tempF4.setForeground(Color.DARK_GRAY); canvas.add(tempF4, 80+xConstant*2, 570+offSetY1);
                //tempC4.setText(w.jseDay3.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC4.setFont(buttonFont); tempC4.setForeground(Color.BLACK); canvas.add(tempC4,85+xConstant*2, 570+offSetY2);
                day4.setText(w.jseDay3.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay3.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay3.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day4.setFont(buttonFont); day4.setForeground(Color.WHITE); canvas.add(day4, 90+xConstant*2, 525);
                day4.setHorizontalAlignment(SwingConstants.CENTER);


                //day 5 observation
                day5Cond = new GImage(w.day4Gif); canvas.add(day5Cond, 80, 770);
                tempF5.setText(w.jseDay4.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay4.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF5.setFont(labelFont); tempF5.setForeground(Color.DARK_GRAY); canvas.add(tempF5, 80, 770+offSetY1);
                //tempC5.setText(w.jseDay4.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC5.setFont(buttonFont); tempC5.setForeground(Color.BLACK); canvas.add(tempC5,85, 770+offSetY2);
                day5.setText(w.jseDay4.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay4.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay4.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day5.setFont(buttonFont); day5.setForeground(Color.WHITE); canvas.add(day5, 90, 725);
                day5.setHorizontalAlignment(SwingConstants.CENTER);

                //day 6 observation
                day6Cond = new GImage(w.day5Gif); canvas.add(day6Cond, 80+xConstant,770);
                tempF6.setText(w.jseDay5.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - "+w.jseDay5.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF6.setFont(labelFont); tempF6.setForeground(Color.DARK_GRAY); canvas.add(tempF6, 80+xConstant,770+offSetY1);
                //tempC6.setText(w.jseDay5.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC6.setFont(buttonFont); tempC6.setForeground(Color.BLACK); canvas.add(tempC6, 85+xConstant,770+ offSetY2);
                day6.setText(w.jseDay5.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay5.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay5.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day6.setFont(buttonFont); day6.setForeground(Color.WHITE); canvas.add(day6, 90+xConstant, 725);
                day6.setHorizontalAlignment(SwingConstants.CENTER);


                //day 7 observation
                day7Cond = new GImage(w.day6Gif); canvas.add(day7Cond, 80+xConstant*2, 770);
                tempF7.setText(w.jseDay6.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay6.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF7.setFont(labelFont); tempF7.setForeground(Color.DARK_GRAY); canvas.add(tempF7, 80+xConstant*2, 770+offSetY1);
                //tempC7.setText(w.jseDay6.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC7.setFont(buttonFont); tempC7.setForeground(Color.BLACK); canvas.add(tempC7, 80+xConstant*2, 770+ offSetY2);
                day7.setText(w.jseDay6.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay6.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay6.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day7.setFont(buttonFont); day7.setForeground(Color.WHITE); canvas.add(day7, 90+xConstant*2, 725);
                day7.setHorizontalAlignment(SwingConstants.CENTER);

                //day 8 observation
                day8Cond = new GImage(w.day7Gif); canvas.add(day8Cond,  80, 970);
                tempF8.setText(w.jseDay7.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay7.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF8.setFont(labelFont); tempF8.setForeground(Color.DARK_GRAY); canvas.add(tempF8, 80, 970+offSetY1);
                //tempC8.setText(w.jseDay7.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC8.setFont(buttonFont); tempC8.setForeground(Color.BLACK); canvas.add(tempC8,  85, 970+offSetY2);
                day8.setText(w.jseDay7.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay7.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay7.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day8.setFont(buttonFont); day8.setForeground(Color.WHITE); canvas.add(day8, 90, 925);
                day8.setHorizontalAlignment(SwingConstants.CENTER);

                //day 9 observation
                day9Cond = new GImage(w.day8Gif); canvas.add(day9Cond, 80+xConstant, 970);
                tempF9.setText(w.jseDay8.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay8.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F");
                tempF9.setFont(labelFont); tempF9.setForeground(Color.DARK_GRAY); canvas.add(tempF9, 80+xConstant, 970+offSetY1);
                //tempC9.setText(w.jseDay8.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //tempC9.setFont(buttonFont); tempC9.setForeground(Color.BLACK); canvas.add(tempC9, 85+xConstant, 970+offSetY2);
                day9.setText(w.jseDay8.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay8.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay8.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day9.setFont(buttonFont); day9.setForeground(Color.WHITE); canvas.add(day9, 90+xConstant, 925);
                day9.setHorizontalAlignment(SwingConstants.CENTER);

                //day 10 observation
                day10Cond = new GImage(w.day9Gif); canvas.add(day10Cond, 80+xConstant*2, 970);
                tempF10.setText(w.jseDay9.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString() + "°F - " + w.jseDay9.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString() + "°F"); tempF10.setFont(labelFont);
                tempF10.setForeground(Color.DARK_GRAY); canvas.add(tempF10, 80+xConstant*2, 970+offSetY1);
                //tempC10.setText(w.jseDay9.getAsJsonObject().get("high").getAsJsonObject().get("celsius").getAsString() + "°C");
                //empC10.setFont(buttonFont); tempC10.setForeground(Color.BLACK); canvas.add(tempC10, 85+xConstant*2, 970+offSetY2);
                day10.setText(w.jseDay9.getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString() + ", " + w.jseDay9.getAsJsonObject().get("date").getAsJsonObject().get("monthname").getAsString() + " " + w.jseDay9.getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString());
                day10.setFont(buttonFont); day10.setForeground(Color.WHITE); canvas.add(day10, 90+xConstant*2, 925);
                day10.setHorizontalAlignment(SwingConstants.CENTER);


                break;
            }
            case "map":
            {
                Boolean mapNull = mapImage == null;
                mapImage = new Map(inputBox).getMapImage();
                if(mapImage!=null)
                {
                    Weather w = new Weather(inputBox);

                    canvas.add(mapImage, radarMapX, radarMapY);
                    //locationText.setText(w.getCityState());
                }
                break;
            }
            /*case "clear":
            {
                zipField.setText("");
                //mapImage.setVisible(false);
                break;
            }*/
        }
    }
}

