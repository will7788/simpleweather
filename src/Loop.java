/**
 * Created by Commander X on 4/30/2017.
 */
public class Loop
{

    public void startLoop()
    {
        long lastTime = System.nanoTime();
        double nsPerTick = 1000000000D/60D; //this value here controls the Update Rate for our loop per second. 60D = 60 updates D just means double. for 240 it would just be 240D.
        int ticks = 0;
        int frames = 0;

        long lastTimer = System.currentTimeMillis();
        double delta = 0;

        //init();
        boolean running = true;
        while(running)
        {
            long now = System.nanoTime();
            delta +=(now - lastTime)/nsPerTick;
            lastTime = now;
            boolean shouldRender = true;

            while(delta >= 1)
            {
                ticks++;
                int mod = ticks % 60;
                if( (mod == 0))


                //locationText.setText("The text changed");

                //tick(); //How often to call this. This is a tick. we can run render speed apart from our tick speeds in this loop.
                // you get to spots to call stuff in this loop this is one off them linked to ticks.

                delta -=1;
                //shouldRender = true;
            }

            try
            {
                Thread.sleep(5); //this will also speed up and slow down the loop
            }

            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            if(shouldRender) //Render block should you wanna control your own image drawing.
            {
                frames++;
                //render(); //render Call or what every else you wanna call every frame or every loop.
            }


            if(System.currentTimeMillis()-lastTimer >= 1000)
            {
                lastTimer += 1000;
                System.out.println(frames + ","+ ticks);
                frames = 0;
                ticks = 0;
            }
        }
    }

}
