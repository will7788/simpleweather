import acm.graphics.GImage;

class Map
{
    private final String apiKey = Main.API_KEY;
    private final String prefix = Main.URL_PREFIX;
    private GImage radarSatellite;

    /**
     * Default constructor.
     * Note: Size defaults to 200x200, not the API default of 300x300.
     * @param query Postal code
     */
    Map(String query)
    {
        this(query, 300, 300);
    }

    /**
     * Basic resizable constructor.
     * @param query
     * @param width API-param default=300
     * @param height API-param default=300
     */
    Map(String query, int width, int height)
    {
        this(query, width, height, false, false);
    }

    /**
     * Full constructor.
     * Does not currently support all of the API params.
     * If value is the API default, it's not included in the query string.
     * @param query
     * @param width API-param default=300
     * @param height API-param default=300
     * @param timelabel API-param default=false
     * @param noclutter API-param default=false
     */
    Map(String query, int width, int height, boolean timelabel, boolean noclutter)
    {
        try
        {
            String url = prefix + apiKey + "/animatedradar/animatedsatellite/q/" + query + ".gif?num=8" + (width == 300 ? "" : "&rad.width=" + width + "&sat.width=" + width) + (height == 300 ? "" : "&rad.height=" + height + "&sat.height=" + height) + (timelabel ? "&sat.timelabel=1" : "") + (noclutter ? "&rad.noclutter=1" : "");
            radarSatellite = new GImage(url);
        }
        catch (java.lang.NumberFormatException meow)
        {

        }
    }

    GImage getMapImage()
    {
        return radarSatellite;
    }
}
