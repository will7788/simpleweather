import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

class Weather
{
    private final String serverAccessKey = Main.API_KEY;
    private final String prefix = Main.URL_PREFIX;
    private JsonParser gParser = new JsonParser();
    private JsonElement JsonObject;
    private JsonElement jseForecast;
    JsonElement jseDay0;
    JsonElement jseDay1;
    JsonElement jseDay2;
    JsonElement jseDay3;
    JsonElement jseDay4;
    JsonElement jseDay5;
    JsonElement jseDay6;
    JsonElement jseDay7;
    JsonElement jseDay8;
    JsonElement jseDay9;
    String day0Gif;
    String day1Gif;
    String day2Gif;
    String day3Gif;
    String day4Gif;
    String day5Gif;
    String day6Gif;
    String day7Gif;
    String day8Gif;
    String day9Gif;


    Weather(int zip)
    {
        String serverPath = prefix + serverAccessKey + "/forecast10day/q/"
                + zip + ".json";

        try
        {
            URL urlObject = new URL(serverPath);

            InputStream is = urlObject.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            JsonObject = gParser.parse(isr);
            //JsonObject = JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject();
        }
        catch (java.net.MalformedURLException mue)
        {
            //do message box here from JOptionPanel or other message failure
        }
        catch (java.io.IOException ioe)
        {
            //d1o message box here from JOptionPanel or other message failure
        }

        getForecast();

    }

    Weather(String zip)
    {
        String serverPath = prefix + serverAccessKey + "/forecast10day/q/"
                + zip + ".json";

        try
        {
            URL urlObject = new URL(serverPath);

            InputStream is = urlObject.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            JsonObject = gParser.parse(isr);
            //JsonObject = JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject();
        }
        catch (java.net.MalformedURLException mue)
        {
            //do message box here from JOptionPanel or other message failure
        }
        catch (java.io.IOException ioe)
        {
            //d1o message box here from JOptionPanel or other message failure
        }

        getForecast();

    }

    void getForecast()
    {
        jseForecast = JsonObject.getAsJsonObject()
                .get("forecast").getAsJsonObject()
                .get("simpleforecast").getAsJsonObject()
                .get("forecastday");

        jseDay0 = jseForecast.getAsJsonArray().get(0);
        day0Gif = jseDay0.getAsJsonObject().get("icon").getAsString();
        day0Gif = ("Conditions Gifs/" + day0Gif + ".gif");

        jseDay1 = jseForecast.getAsJsonArray().get(1);
        day1Gif = jseDay1.getAsJsonObject().get("icon").getAsString();
        day1Gif = ("Conditions Gifs/" + day1Gif + ".gif");

        jseDay2 = jseForecast.getAsJsonArray().get(2);
        day2Gif = jseDay2.getAsJsonObject().get("icon").getAsString();
        day2Gif = ("Conditions Gifs/" + day2Gif + ".gif");

        jseDay3 = jseForecast.getAsJsonArray().get(3);
        day3Gif = jseDay3.getAsJsonObject().get("icon").getAsString();
        day3Gif = ("Conditions Gifs/" + day3Gif + ".gif");

        jseDay4 = jseForecast.getAsJsonArray().get(4);
        day4Gif = jseDay4.getAsJsonObject().get("icon").getAsString();
        day4Gif = ("Conditions Gifs/" + day4Gif + ".gif");

        jseDay5 = jseForecast.getAsJsonArray().get(5);
        day5Gif = jseDay5.getAsJsonObject().get("icon").getAsString();
        day5Gif = ("Conditions Gifs/" + day5Gif + ".gif");

        jseDay6 = jseForecast.getAsJsonArray().get(6);
        day6Gif = jseDay6.getAsJsonObject().get("icon").getAsString();
        day6Gif = ("Conditions Gifs/" + day6Gif + ".gif");

        jseDay7 = jseForecast.getAsJsonArray().get(7);
        day7Gif = jseDay7.getAsJsonObject().get("icon").getAsString();
        day7Gif = ("Conditions Gifs/" + day7Gif + ".gif");

        jseDay8 = jseForecast.getAsJsonArray().get(8);
        day8Gif = jseDay8.getAsJsonObject().get("icon").getAsString();
        day8Gif = ("Conditions Gifs/" + day8Gif + ".gif");

        jseDay9 = jseForecast.getAsJsonArray().get(9);
        day9Gif = jseDay9.getAsJsonObject().get("icon").getAsString();
        day9Gif = ("Conditions Gifs/" + day9Gif + ".gif");

    }

    /*String getConditionsGif() //Returns the path of the conditions gif
    {
        String filename = JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
        filename = ("Conditions Gifs/" + filename + ".gif");
        return filename;
    }

    double getTempf()
    {
        return Double.parseDouble(JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString());
    }

    double getTempc()
    {
        return Double.parseDouble(JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_c").getAsString());
    }

    String getWeather()
    {
        return JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    String getCityState()
    {
        return JsonObject.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
    }*/

}